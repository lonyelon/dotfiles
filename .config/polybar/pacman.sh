#!/bin/sh

pac=$(checkupdates | wc -l)
aur=$(cower -u | wc -l)

if [[ "$((pac + aur))" != "0" ]]; then
    echo "${pac} - ${aur}"
fi

