#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias wine32='env WINEARCH=win32 WINEPREFIX="$HOME/.wine32" wine'
alias clip='xclip -selection clipboard'

eval $(thefuck --alias)

export QT_QPA_PLATFORMTHEME=gtk2

export VISUAL=vim
export EDITOR=vim

export HISTCONTROL=ignoreboth:erasedups

export THEFUCK_NO_COLORS=true
export THEFUCK_REQUIRE_CONFIRMATION=false

export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin

export PS1="\[\$(astra)\]\e[0m[\[\e[38;5;161m\e[3m\]\u\[\e[0m\]@\[\e[38;5;80m\e[3m\]\h\[\e[0m\]] \[\e[38;5;146m\]\w\[\e[0m\]\n\[\e[38;5;42m\e[1m\]\$\[\e[0m\] "

export PATH="/home/inori/.gem/ruby/2.4.0/bin:/home/inori/go/bin:$PATH"
export PERL5LIB="/home/inori/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"
export LD_LIBRARY_PATH="/opt/cuda/lib64"

[ -e "/etc/DIR_COLORS" ] && DIR_COLORS="/etc/DIR_COLORS"
[ -e "$HOME/.dircolors" ] && DIR_COLORS="$HOME/.dircolors"
[ -e "$DIR_COLORS" ] || DIR_COLORS=""
eval "`dircolors -b $DIR_COLORS`"

source /usr/share/bash-completion/bash_completion
source /usr/share/doc/pkgfile/command-not-found.bash
