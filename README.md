# Lonyelon's dotfiles

## Required packages
This is a list of packages you will need to use this dotfiles.
```
i3-gapps
i3blocks
dmenu
polybar
rofi
```

## Usage

1. Backup your $Documents$ folder
2. Execute $install.sh$
3. DONE!
